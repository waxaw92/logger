package com.belatrix.logger.enums;

public enum StoreLoggerEnum {

    DB_STORE,
    CONSOLE_STORE,
    FILE_STORE;

}
