package com.belatrix.logger.enums;

import java.util.logging.Level;

public enum LevelLoggerEnum {

    LEVEL_MESSAGE(1, "MESSAGE", Level.INFO),
    LEVEL_ERROR(2, "ERROR", Level.SEVERE),
    LEVEL_WARNING(3, "WARNING", Level.WARNING);

    private final Integer id;
    private final String description;
    private final Level level;

    private LevelLoggerEnum(Integer id, String description, Level level) {
        this.id = id;
        this.description = description;
        this.level = level;
    }

    public Integer getId() {
        return this.id;
    }

    public String getDescription () {
        return this.description;
    }

    public Level getLevel() {
        return this.level;
    }

}
