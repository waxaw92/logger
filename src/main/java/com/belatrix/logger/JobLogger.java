package com.belatrix.logger;

import com.belatrix.logger.enums.LevelLoggerEnum;
import com.belatrix.logger.enums.StoreLoggerEnum;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JobLogger {

    private StoreLoggerEnum store;
    private Map<String, String> dbParams;
    private final Logger LOGGER;

    public static final String FILE_FOLDER_PROPERTY = "logFileFolder";
    public static final String DB_URL_CONNECTION= "dbConnection";
    public static final String USER = "userName";
    public static final String PASS = "password";

    public JobLogger(StoreLoggerEnum store, Map<String, String> dbParams) {
        LOGGER = Logger.getLogger("MyLog");
        this.store = store;
        this.dbParams = dbParams;
    }

    public void logMessage(String messageText, LevelLoggerEnum level) throws Exception {

        if (messageText == null || messageText.length() == 0)
            return;

        if (level == null)
            throw new Exception("Invalid configuration");

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG);
        String message = String.format("[%s - %s]: %s", level.getDescription(), dateFormat.format(new Date()), messageText);

        switch (store) {
            case DB_STORE:
                dbLog(message, level);
                break;
            case FILE_STORE:
                fileLog(message, level);
                break;
            default:
                consoleLog(message, level);
                break;
        }

    }

    private void consoleLog(String message, LevelLoggerEnum level) {
        ConsoleHandler ch = new ConsoleHandler();
        LOGGER.addHandler(ch);
        LOGGER.log(level.getLevel(), message);
    }

    private void fileLog(String message, LevelLoggerEnum level) {
        try {
            File logFile = new File(dbParams.get(FILE_FOLDER_PROPERTY) + "/logFile.txt");
            if (!logFile.exists())
                logFile.createNewFile();

            FileHandler fh = new FileHandler(dbParams.get("logFileFolder") + "/logFile.txt");
            LOGGER.addHandler(fh);
            LOGGER.log(level.getLevel(), message);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error creando log en archivo fisico.");
        }
    }

    private void dbLog(String message, LevelLoggerEnum level) {
        try {
            Properties connectionProps = new Properties();
            connectionProps.put("user", dbParams.get(USER));
            connectionProps.put("password", dbParams.get(PASS));
            String databaseUrl = String.format("jdbc:%s", dbParams.get(DB_URL_CONNECTION));
            String insert = "insert into log_ values( ?, ?)";
            logMessageDB(databaseUrl, connectionProps, insert, message, level);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error generando log en base de datos.");
        }
    }

    private void logMessageDB (String databaseUrl, Properties connectionProps,
                                     String insert, String message, LevelLoggerEnum level) throws SQLException {
        try (Connection connection = DriverManager.getConnection(databaseUrl, connectionProps);
             PreparedStatement statement = connection.prepareStatement(insert)) {
            statement.setString(1, message);
            statement.setString(2, level.getId().toString());
            statement.executeUpdate();
        }
    }

}
