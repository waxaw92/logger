package com.belatrix.logger;

import com.belatrix.logger.enums.LevelLoggerEnum;
import com.belatrix.logger.enums.StoreLoggerEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class JobLoggerTest {

    private Map<String, String> dbParams;
    private JobLogger jobLogger;

    @BeforeEach
    public void init() {
        dbParams = new HashMap<>();
        dbParams.put(JobLogger.DB_URL_CONNECTION, "h2:mem:test");
        dbParams.put(JobLogger.USER, "sa");
        dbParams.put(JobLogger.PASS, "");
    }

    @Test
    public void consoleLogTest() throws Exception {
        jobLogger = new JobLogger(StoreLoggerEnum.CONSOLE_STORE, dbParams);
        jobLogger.logMessage("Mensaje informativo, log en consola", LevelLoggerEnum.LEVEL_MESSAGE);
    }

    @Test
    public void fileLogTest() throws Exception {
        String tmpFolder = System.getProperty("java.io.tmpdir");
        dbParams.put(JobLogger.FILE_FOLDER_PROPERTY, tmpFolder);
        jobLogger = new JobLogger(StoreLoggerEnum.FILE_STORE, dbParams);
        jobLogger.logMessage("Mensaje informativo, log en archivo", LevelLoggerEnum.LEVEL_WARNING);
        File logFile = new File(dbParams.get(JobLogger.FILE_FOLDER_PROPERTY) + "/logFile.txt");
        Assertions.assertTrue(logFile.exists());
    }

    @Test
    public void dbLogTest() throws Exception {
        Class.forName("org.h2.Driver");
        createDatabase();
        jobLogger = new JobLogger(StoreLoggerEnum.DB_STORE, dbParams);
        jobLogger.logMessage("Mensaje informativo, log en base de datos", LevelLoggerEnum.LEVEL_ERROR);
        validateLogDatabase();
    }

    private void createDatabase() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "sa");
        connectionProps.put("password", "");
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:test", connectionProps);
        PreparedStatement statement = connection.prepareStatement("CREATE TABLE log_ (mensaje VARCHAR(200), level INT);");
        statement.executeUpdate();
    }

    private void validateLogDatabase() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "sa");
        connectionProps.put("password", "");
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:test", connectionProps);
        PreparedStatement statement = connection.prepareStatement("SELECT count(1) FROM log_ WHERE level = 2;");
        ResultSet results = statement.executeQuery();
        Assertions.assertNotNull(results);
        results.first();
        Integer rows = results.getInt(1);
        Assertions.assertNotNull(rows);
        Assertions.assertEquals(rows, 1);
    }

}
