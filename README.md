# Logger

### Project structure

The project has two folders where it was organized: **src/main** and **src/test**.
The files within those folders were distributed as follows:

>
> + src/main/
>	+ java/com/belatrix/logger/
>		+ enums/
>			+ LevelLoggerEnum.java
>			+ StoreLoggerEnum.java
>		+ JobLogger.java
>		+ JobLogger.java.orig
> + test/java/com/belatrix/logger/
>	+ JobLoggerTest.java
> + pom.xml
>

As you can see, the project contains two similar files **JobLogger.java** and **JobLogger.java.orig**. In the **JobLogger.java.orig**
you will find the original class code with the comments about best practices. Whereas, in the **JobLogger.java** you will find the class
using those recomendations.

### Libs and more

The project was made using *__maven__* and adding the **junit jupiter** dependency as you can see in the *pom.xml* file.
Furthermore was configurated to use Java 8.

### The repository

All the code was made on the *__develop__* branch and merged on the *__master__* branch, so both branches have the same code.